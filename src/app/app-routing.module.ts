import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaloriesListComponent } from './calories-list/calories-list.component';
import { EditMealComponent } from './edit-meal/edit-meal.component';
import { MealResolverService } from './edit-meal/meal-resolver.service';

const routes: Routes = [
  {path: '', component: CaloriesListComponent},
  {path: 'edit-meal', component: EditMealComponent},
  {path: ':id/edit-meal', component: EditMealComponent,
  resolve: {meal: MealResolverService}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

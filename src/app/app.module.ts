import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditMealComponent } from './edit-meal/edit-meal.component';
import { CaloriesListComponent } from './calories-list/calories-list.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MealService } from './shared/meal.service';



@NgModule({
  declarations: [
    AppComponent,
    EditMealComponent,
    CaloriesListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }

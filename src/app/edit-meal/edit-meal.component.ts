import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MealService } from '../shared/meal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from '../shared/meal.model';

@Component({
  selector: 'app-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit, OnDestroy {
  @ViewChild('m')mealForm!: NgForm;
  isUpLoading = false;
  mealUploadingSubscription!: Subscription;
  isEdit = false;
  editedId = '';

  constructor(private mealService: MealService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.mealUploadingSubscription = this.mealService.mealUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });

    this.route.data.subscribe(data => {
      const meal = <Meal | null> data.meal;
      if (meal){
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          category: meal.category,
          text: meal.text,
          calories: meal.calories
        })

      }  else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          category: '',
          text: '',
          calories: ''
        })

      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }

  saveMeal(){
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(id, this.mealForm.value.category, this.mealForm.value.text, this.mealForm.value.calories)
    const next = () => {
      this.mealService.fetchMeals();
      void this.router.navigate(['..'], {relativeTo: this.route});
    };

    if (this.isEdit){
      this.mealService.editMeal(meal).subscribe(next);
    } else{
      this.mealService.addMeal(meal).subscribe(next);
    }
  }

  ngOnDestroy(): void {
    this.mealUploadingSubscription.unsubscribe();
  }

}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Meal } from '../shared/meal.model';
import { EMPTY, Observable, of } from 'rxjs';
import { MealService } from '../shared/meal.service';
import { HttpClient } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MealResolverService implements Resolve<Meal>{

  constructor(private mealService: MealService, private router: Router, private http: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never> {
    const mealId = <string>route.params['id'];
    return this.mealService.fetchMeal(mealId).pipe(mergeMap(dish => {
      if (dish){
        return of (dish);
      }
      void this.router.navigate(['/dishes']);
      return EMPTY;
    }));
  }
}

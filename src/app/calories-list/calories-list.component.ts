import { Component, OnDestroy, OnInit } from '@angular/core';
import { MealService } from '../shared/meal.service';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-calories-list',
  templateUrl: './calories-list.component.html',
  styleUrls: ['./calories-list.component.css']
})
export class CaloriesListComponent implements OnInit, OnDestroy {
  meals: Meal[] = [];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  mealRemovingSubscription!: Subscription;
  isFetching = false;
  isRemoving = false;

  constructor(private mealService: MealService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });

    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealRemovingSubscription = this.mealService.mealRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
    this.mealService.fetchMeals();
  }

  getTotal() {
    return this.meals.reduce((acc, item) => {
      return acc + item.calories;
    }, 0);
  }

  onRemove(id: string){
    this.mealService.removeMeal(id).subscribe(() =>{
      this.mealService.fetchMeals();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

  ngOnDestroy(): void {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
    this.mealRemovingSubscription.unsubscribe();
  }

}

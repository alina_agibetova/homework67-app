import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Meal } from './meal.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';

@Injectable()
export class MealService {
  mealsChange = new Subject<Meal[]>();
  mealUpLoading = new Subject<boolean>();
  mealsFetching = new Subject<boolean>();
  mealRemoving = new Subject<boolean>();
  private meals: Meal[] = [];
  private category = 'breakfast';

  constructor(private http: HttpClient, private route: ActivatedRoute){}

  fetchMeals(){
    this.mealsFetching.next(true);
    this.http.get<{[id: string]: Meal}>('https://alina-beaf9-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const data = result[id];

          return new Meal(id, data.category, data.text, data.calories);
        })
      })).subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice());
        this.mealsFetching.next(false);
    }, error => {
        this.mealsFetching.next(false);
    })

  }
  getMeals(){
    return this.meals.slice();
  }

  fetchMeal(id: string){
    return this.http.get<Meal | null >(`https://alina-beaf9-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      map(result => {
        if (!result){
          return null;
        }

        return new Meal(id, result.category, result.text, result.calories);
      })
    );
  }

  addMeal(meal: Meal){
    const body = {
      category: meal.category,
      text: meal.text,
      calories: meal.calories
    };

    this.mealUpLoading.next(true);

    return this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/meals.json', body).pipe(
      tap(() => {
        this.mealUpLoading.next(false);
      }, () => {
        this.mealUpLoading.next(false);
      })
    )
  }

  editMeal(meal: Meal){
    this.mealUpLoading.next(true);
    const body = {
      category: meal.category,
      text: meal.text,
      calories: meal.calories
    };

    return this.http.put(`https://alina-beaf9-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body).pipe(
      tap(() => {
        this.mealUpLoading.next(false);
      }, ()=> {
        this.mealUpLoading.next(false);
      })
    );
  }

  removeMeal(id: string){
    console.log(id);
    this.mealRemoving.next(true);
    return this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      tap(() => {
        this.mealRemoving.next(false);
      }, () => {
        this.mealRemoving.next(false);
      })
    );
  }


  getCategories(id: string){
    this.route.params.subscribe((params: Params)=> {
      const name = params['name'];
      this.category = name || 'breakfast';
      this.meals = [];

      let url = 'https://alina-beaf9-default-rtdb.firebaseio.com/meals.json';

      if (name){
        url += `?orderBy="category"&equalTo="${name}"`
      }

      this.http.get<{[id: string]: Meal}>(url).pipe(
        map(result => {
          return Object.keys(result).map(id => {
            const categoryData = result[id];
            return new Meal(id, categoryData.category, categoryData.text, categoryData.calories)
          })
        })
      )
        .subscribe(meals => {
        this.meals = meals;
      })

    })

  }

}

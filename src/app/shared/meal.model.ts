export class Meal {
  constructor(
    public id: string,
    public category: string,
    public text: string,
    public calories: number,
  ){}

}
